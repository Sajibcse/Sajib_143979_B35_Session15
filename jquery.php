<!DOCTYPE html>
    <html>
        <head>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

            <script language="JavaScript">
                $(document).ready
                (
                    function ()
                    {
                    $("#P1").delay(1000).fadeOut("slow");
                    }
                );
            </script>
        </head>

        <body>
            <p id="P1">You don't need to click on me, I'll disappear on my own!</p>
        </body>
    </html>